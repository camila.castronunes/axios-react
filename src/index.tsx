import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Post from "./components/Post";
import PostId from "./components/PostId";
import reportWebVitals from "./reportWebVitals";
import Users from "./components/Users";
import UserId from "./components/UserId";
import PostForms from "./components/PostForms";
import UserFormsById from "./components/UserForms";
import AxiosPostUser from "./components/AxiosMetodoPostUser";



const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    {/* <Users />
    <Post />
    <PostId />
    <UserId/> */}
    {/* <PostForms/> */}
    {/* <UserFormsById/> */}
    <AxiosPostUser/>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
