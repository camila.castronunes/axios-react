import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import axios, { CancelTokenSource } from "axios";
import "../App.css"

// Verbos http tem atributos. Uma boa prática: criar uma interface
// Um post: tem id, title, body e userId opcional
interface IPostId {
  id: number;
  userId?: number;
  title: string;
  body: string;
}

// Uma variável default para post, que vai receber o estado do post
//   pode ser populada após chamada da API
const defaultPosts: IPostId = { id: 5, title: "", body: "" };

// Um componente funcional React, que vai consumir a API
function PostById() {

  const [postId, setPostId]: [IPostId, (id: IPostId) => void] =
    React.useState(defaultPosts);


  React.useEffect(() => {
    axios
      .get<IPostId>(`https://jsonplaceholder.typicode.com/posts/${postId.id}`, {
        headers: { "Content-Type": "application/json" },
        timeout: 10000,
      })
      .then((response) => {
        console.log(response.data);
        setPostId(response.data);
      })
      .catch((ex) => {
        const error =
          ex.response.status === 404
            ? "Resource Not found"
            : "Ocorreu um erro inesperado";
      });
  }, []);

  return (
    <div className="App">
      <ul>
        <li key={postId.id}>
          <h3>{postId.title}</h3>
          <p>{postId.body}</p>
        </li>
      </ul>
    </div>
  );
}

export default function PostId() {
  return (
    <>
      <hr />
      <h1>Posts por id</h1>
      <PostById />
    </>
  );
}
