import React, { ChangeEvent, createElement, SyntheticEvent, useState } from "react";
import axios, { CancelTokenSource } from "axios";
import "../App.css";
import { error } from "console";

interface IPostForm {
  id: string;
  userId?: number;
  title: string;
  body: string;
}



function PostFormsAxios() {


    const defaultPosts: IPostForm = { id: "", title: "", body: "" };
    const [postForm, setPostForm]: [IPostForm, (post: IPostForm) => void] =
    useState(defaultPosts);



  const [id, setId]: [string, (id: string) => void] = useState("");
  const [title, setTitle]: [string, (id: string) => void] = useState("");
  const [body, setBody]: [string, (id: string) => void] = useState("");
  const [erro, setErro]: [string, (erro: string)=>void] = useState("");
  // const [aux, setAux]: [boolean, (aux: boolean)=>void] = useState(false)

  const prevent = (event: React.SyntheticEvent) => {
    event.preventDefault();
   
    
  };


const render = ()=>{
    setTitle(postForm.title)
    setBody(postForm.body)
    setId("")  
     
}
  


     React.useEffect(() => {
      if(id !== ""){ //para não puxar tudo quando for id for vazio
      axios
        .get<IPostForm>(`https://jsonplaceholder.typicode.com/posts/${id}`, {
          headers: { "Content-Type": "application/json" },
          timeout: 10000,
        })
        
        .then((response) => {
          console.log(response.data);
          setPostForm(response.data); //pega tudo do id
          setErro("")
        })
        .catch((ex) => {
          setErro(
            ex.response.status === 404
              ? "Resource Not found"
              : "Ocorreu um erro inesperado");

        });
      }
    },[id]); //Muda a renderização de acordo com a mudança do id
  
    

  return (
    <div>
    <h1>Postagem por id</h1>
    
      <form onSubmit={prevent}>
        <label>Digite o id desejado:</label>
        <input
          type="number"
          value={id}
          onChange={(e) => setId(e.target.value)}
        />
       
       
        <button onClick={render} className="btn">Pesquisar</button>
         </form>
        
        <div className={erro.length === 0 ? "ok" : "deuerro"}>

       
            <h3>Título do post</h3>
            <h4>{title}</h4>
            <h3>Postagem:</h3>
            <h4>{body}</h4>

          </div>
          
          {erro &&
              
           <h4 className="erro">{erro}</h4>
          }
               
        
       
     
    </div>
  );
}

export default function PostForms() {
  return (
    <>
      <PostFormsAxios/>

      
    </>
  );
}
