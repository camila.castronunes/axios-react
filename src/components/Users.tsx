import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import axios, { CancelTokenSource } from 'axios';
import "../App.css"


interface IUser{
    id: number;
    name: string;
    username: string;
    email: string 
}


const defaultUsers : Array<IUser> = [];


function UsersAxios(){

    const [users,setUsers] : [Array<IUser> , (users: Array<IUser>) => void] = useState(defaultUsers);


    React.useEffect(()=>{
        axios.get<Array<IUser>>('http://jsonplaceholder.typicode.com/users',
            {headers: {'Content-Type': 'application/json'}, timeout: 10000})
            .then((response) => {
            console.log(response.data);
            setUsers(response.data)
            })
            .catch((ex)=>{
                console.log(`${ex.response.status}`)
            })
    },[]);


    return(
        <div>
            <h1>Users</h1>
            <ul>
                {users.map((user)=>(
                    <li key={user.id}><h4>{user.name}</h4>
                    <p>Username:{user.username}</p>
                    <p>E-mail:{user.email}</p>
                    </li>
                )       
                )}                
            </ul>
        </div>
    )
}


export default function Users(){

    return(
        <>
            <UsersAxios/>
        
        </>
    )

}