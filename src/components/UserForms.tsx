import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import axios from "axios";
import "../App.css";

interface IUser {
  id: string;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
  phone: string;
  website: string;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  };
}

const defaultUser: IUser = {
  id: "",
  name: "",
  username: "",
  email: "",
  address: {
    street: "",
    suite: "",
    city: "",
    zipcode: "",
    geo: {
      lat: "",
      lng: "",
    },
  },
  phone: "",
  website: "",
  company: {
    name: "",
    catchPhrase: "",
    bs: "",
  },
};

function UserForms() {
  const [idUser, setIdUser] = useState("");
  const [user, setUser]: [IUser, (user: IUser) => void] = useState(defaultUser);
  const [erro, setErro]: [string, (erro: string) => void] = useState("");

  const submit = (event: React.SyntheticEvent) => {
    event.preventDefault();

    axios
      .get<IUser>(`https://jsonplaceholder.typicode.com/posts/${idUser}`, {
        headers: { "Content-Type": "application/json" },
        timeout: 10000,
      })
      .then((response) => {
        console.log(response.data);
        setUser(response.data);
      })
      .catch((ex) => {
        setErro(
          ex.response.status === 404
            ? "Resource Not found"
            : "Ocorreu um erro inesperado"
        );
      });
  };

  return(

    <div>

        <h1>Localize o usuário por ID: </h1>
        <form className="formsUser">
            <div>
                <label> Digite o Id desejado: </label>
                <input
                    type="number"
                    value={idUser}
                    onChange={(e)=> setIdUser(e.target.value)}            
                 />
            </div>
            <hr/>

            <div className="resultado">
                <h3>Nome:</h3>
                <h4></h4>

            </div>

        </form>



    </div>
  )
     
   
   ;
}


export default function UserFormsById() {
    return (
      <>
        
        <UserForms/>
      </>
    );
  }