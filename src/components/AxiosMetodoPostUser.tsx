import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import axios, { CancelTokenSource } from 'axios';
import "../App.css"




interface IUser{
    id: string;
    name: string;
    username: string;
    email: string 
}

const defaultUser : IUser = {id:"", name : "", username: "", email: ""}

function PostUserAxios(){

    const url = "https://jsonplaceholder.typicode.com/users";

    const [user,setUser] : [IUser, (user : IUser) => void] = useState(defaultUser);
    
    const [name, setName] : [string, (name : string) => void] = useState("");
    const [userName, setUserName] : [string, (userName : string) => void] = useState("");
    const [email, setEmail] : [string, (email : string) => void] = useState("");
    const [erro, setErro]: [string, (erro: string)=>void] = useState("");


    const submit = (event: React.SyntheticEvent) =>{
        event.preventDefault()       

        
            axios.post(url, { name: name, username : userName, email: email})
            .then((response)=>{                
                setUser(response.data);  //seta o que foi enviado no user
                console.log(response.data);
                console.log(response.status)               
                
            })
            .catch((ex) => {
                let cod = ex.response.status
                if(cod !== 201)
                    setErro(`Ocorreu um erro inesperado -> ${cod}`)
                    console.log(erro)
                     ;
                })

                resetInput()        
            
    }

    function resetInput (){
        setEmail("")
        setName("")
        setUserName("")
    }

    // const [postagem]

    return(
        <>
        <div>
            <h1 className="h1User">Postagem de Usuário</h1>
            <form onSubmit={submit} className="formsUser">
                <label>Nome:</label>
                <input
                   autoFocus
                    required
                    type ="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    
                />
                 <label>UserName:</label>
                <input
                    required
                    type ="text"
                    value={userName}
                    onChange={(e) => setUserName(e.target.value)}
                    
                />
                 <label>E-mail:</label>
                <input
                    type ="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                />
                <input type="submit" />
            </form>

            <div>
                
                <h3>Id:</h3>
                <h4>{user.id}</h4>
                <h3>Nome:</h3>
                <h4>{user.name}</h4>
                <h3>Username:</h3>
                <h4>{user.username}</h4>
                <h3>E-mail:</h3>
                <h4>{user.email}</h4>
                
            </div>
        </div>
        
        </>
    )
}

export default function AxiosPostUser(){

    return(
        <PostUserAxios/>
    )
}