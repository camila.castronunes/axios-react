import React, { ChangeEvent, SyntheticEvent, useState } from "react";
import axios, { CancelTokenSource } from 'axios';
import "../App.css"


interface IUserId{
    id: number;
    name: string;
    username: string;
    email: string 
}


const defaultUser : IUserId = {"id": 5, "name" : "", "username": "", "email": ""}


function UserById(){

    const [userId, setUserId] : [IUserId, (user: IUserId) => void] = useState(defaultUser);


    React.useEffect(()=>{
        axios.get<IUserId>(`http://jsonplaceholder.typicode.com/users/${userId.id}`,
        {headers: {'Content-Type': 'application/json'}, timeout: 10000})
        .then((response) =>{
                setUserId(response.data)
            })
        .catch((ex)=>{
                console.log(`${ex.response.status}`)   
        })   
    },[]);



    return(
        <>
        <h1>Users por Id</h1>
        <div>
            <ul>
                <li key={userId.id}>
                    <h3> {userId.name}</h3> 
                    <p>username: {userId.username}</p>
                    <p>email: {userId.email}</p>               
                </li>
            </ul>
        </div>
        </>
    )
}


export default function UserId(){

    return(
        <>
        <hr/>
        <UserById/>
        </>
    )

}
